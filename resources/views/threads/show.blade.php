@extends('layouts.master')
@section('content')
    <div class="container">
        @isset($result)
            <h3>{{ $result->title }}</h3>
            <div class="card grey lighten-4">
                <div class="card-content">
                    {{ $result->body }}
                </div>
            </div>

            <replies
                replied="{{ __('replied') }}"
                reply="{{ __('reply') }}"
                your-answer="{{ __('your answer') }}"
                send="{{ __('send') }}"
            >
                @include('layouts.preloader')
            </replies>
        @endisset
    </div>
@endsection
@push('scripts')
    <script src="{{ asset('js/replies.js') }}"></script>
@endpush
