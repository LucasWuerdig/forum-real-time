@extends('layouts.master')
@section('content')
    <div class="container">
        <h3>{{ __('Most recent threads') }}</h3>
        <threads
            title="{{ __('threads') }}"
            threads="{{ __('threads') }}"
            replies="{{ __('replies') }}"
            open="{{ __('open') }}"
        >
            @include('layouts.preloader')
        </threads>
        @isset($result)
            <h3>{{ $result->title }}</h3>
            <div class="card grey lighten-4">
                <div class="card-content">
                    {{ $result->body }}
                </div>
            </div>

            <replies>
                @include('layouts.preloader')
            </replies>
        @endisset
    </div>
@endsection
@push('scripts')
{{--    <script src="{{ asset('js/replies.js') }}"></script>--}}
    <script src="{{ asset('js/threads.js') }}"></script>
@endpush
