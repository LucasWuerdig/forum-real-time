<footer class="page-footer">
    <div class="container">
        &copy; Copyright {{ date('Y') }} - <a href="google.com.br" class="grey-text text-lighten-4 right">Lucas Wuerdig</a>
    </div>
</footer>
